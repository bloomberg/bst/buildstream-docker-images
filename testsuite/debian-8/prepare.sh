#!/bin/sh

set -eu

apt-get update

# BuildStream dependencies
apt-get install -y --no-install-recommends \
        bash-completion bubblewrap bzr findutils fuse git lzip make \
        patch python3-pip python3-gi python3-ruamel.yaml python3-psutil

# Debian-jessie is outdated enough that we need to use pip for these
# dependencies; python3-gi and python3-ruamel.yaml however seem best
# installed through the OS package
pip3 install -U pip
pip3 install arpy blessings click jinja2 markupsafe pluginbase

# Test suite dependencies
pip3 install apipkg coverage==4.4.0 pytest-cov pytest-datafiles execnet \
     pytest-env pep8 pytest pytest-pep8 pytest-pylint pytest-xdist setuptools_scm \
     typing
pip3 install -U setuptools
