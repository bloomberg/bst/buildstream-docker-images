#!/bin/sh

# Ostree build dependencies
apt-get install -y --no-install-recommends \
        bison libglib2.0-dev liblzma-dev e2fslibs-dev \
        libgirepository1.0-dev libgpgme11-dev libfuse-dev \
        libsoup2.4-dev wget xz-utils

# Build and install ostree
cd ~ || exit 1

wget "https://github.com/ostreedev/ostree/releases/download/v2017.8/libostree-2017.8.tar.xz"
tar xf libostree-2017.8.tar.xz
cd libostree-2017.8 || exit 1

./configure --prefix=/usr --enable-introspection --with-soup
make
make install
