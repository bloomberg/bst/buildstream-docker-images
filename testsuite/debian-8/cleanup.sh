#!/bin/sh

set -eu

echo "Removing build dependencies"
apt-get remove -y --purge bison libglib2.0-dev liblzma-dev e2fslibs-dev \
        libgirepository1.0-dev libgpgme11-dev libfuse-dev wget xz-utils \
        libicu52 libgtk2.0-common binutils

echo "Removing apt cache"
apt-get clean
rm -rf /var/lib/apt/lists

rm -R /root/*
