BuildStream Docker images
=========================

The BuildStream project provides
[Docker images](https://hub.docker.com/r/buildstream/)
that contain the BuildStream build tool.

This repo contains the build instructions and automation that produces the
official BuildStream Docker images.

How to build the images
-----------------------

The images are built by the GitLab CI pipeline for this repository, and pushed
automatically to the Docker Hub from there. See `.gitlab-ci.yml` for how this
is implemented.

You can also build one of the images locally for testing purposes. Here is an
example of how to do that for the 'fedora' image. These commands must be run
from the toplevel directory of the repository.

    git clone https://gitlab.com/buildstream/buildstream.git
    mv buildstream fedora/
    docker build fedora --tag "buildstream-fedora:my-test-version"

Running BuildStream inside Docker
---------------------------------

BuildStream does not require Docker for general use. However, not all operating
systems provide the necessary dependencies to run BuildStream, and using a
Docker image can sometimes work around the difficulties that you might have in
getting BuildStream to run.  In particular, Docker allows Mac OS X users to
conveniently run BuildStream builds in a minimal Linux container.

For instructions on using BuildStream with Docker, please see the
[BuildStream manual](https://buildstream.gitlab.io/buildstream/install.html).

The Docker images can also used as base images for automated tests that run
BuildStream builds.
This is done in the tests for
[BuildStream itself](https://gitlab.com/BuildStream/buildstream/blob/master/.gitlab-ci.yml)
and the
[BuildStream integration tests](https://gitlab.com/BuildStream/buildstream-tests/blob/master/.gitlab-ci.yml).


Setting up a BuildStream artifact cache with Docker
---------------------------------------------------

You can set up a [shared artifact
cache](https://buildstream.gitlab.io/buildstream/artifacts.html#artifacts) so
that multiple BuildStream users and/or build workers can share artifacts
they produce.

The buildstream/artifact-cache Docker image provides a simple way to set one
of these up. Run this to start an artifact share:

    docker volume create artifact-cache-config
    docker volume create artifact-cache-data
    docker run --publish 8080:8080 --publish 22200:22200 \
               --volume artifact-cache-config:/home/artifacts/.config \
               --volume artifact-cache-data:/data \
            buildstream/artifact-cache:latest

Once this is running, you should be able to browse to http://localhost:8080/
and see a rather unexciting directory listing page.

To enable pushing to the cache, you need to authorize one or more SSH keys.
This is done by adding the public part of a key to the file
`ssh/authorized_keys` inside the `artifact-cache-config` volume.

You can script that as follows (assuming the key is `/home/me/.ssh/id_rsa.pub`):

    volume=$(docker volume inspect artifact-cache-config | jq -r .[0].Mountpoint)
    cat /home/me/.ssh/id_rsa.pub > ${volume}/ssh/authorized_keys

You need to run this snippet as a user who has write access to the Docker
volume.

To configure BuildStream to push and pull artifacts from this cache, add the
following to your `~/.config/buildstream.conf` file:

    artifacts:
      url: ssh://artifacts@localhost:22200/artifacts/

For pull-only access use:

    artifacts:
      url: http://localhost:8080/artifacts/

Note projects can override your global artifact cache configuration. In these
cases you need to override with:

    project:
      <project name>:
        artifacts:
          ...

This artifact cache should only be used for local testing or within trusted
private networks. If you are going to serve artifacts elsewhere you need to
use HTTPS to serve the artifacts or else you cannot trust that the data is not
being modified in transit.

You may be able to configure the NGINX server within the image to [serve over
SSL](https://www.nginx.com/resources/admin-guide/nginx-ssl-termination/).
Alternately, you can put the cache behind a proxy such as HAProxy and configure
that to do SSL termination.
