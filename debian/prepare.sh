#!/bin/sh

set -eu

apt update

# Buildstream base dependencies
apt install -y --no-install-recommends \
        fuse \
        python3 python3-pip python3-gi python3-psutil \
        python3-ruamel.yaml
apt install -y -t stretch-backports \
        ostree gir1.2-ostree-1.0 bubblewrap

# Buildstream plugins base dependencies
apt install -y --no-install-recommends \
        git bzr tar lzip patch

# Buildstream utilities
apt install -y --no-install-recommends \
        bash-completion

# BuildStream python dependencies
pip3 install setuptools
pip3 install blessings click jinja2 pluginbase ruamel.yaml

# BuildStream plugin python dependencies
pip3 install arpy


# Test suite dependencies
pip3 install apipkg coverage==4.4.0 execnet pep8 pytest pytest-cache pytest-cov \
        pytest-datafiles pytest-env pytest-pep8 pytest-xdist pytest-pylint \
        setuptools_scm

# Needed to build the docs in the CI
apt install -y --no-install-recommends \
        make
