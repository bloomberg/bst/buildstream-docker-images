#!/bin/sh

set -eu

# Buildstream base dependencies
dnf install -y fuse ostree bubblewrap \
               python3 python3-pip python3-gobject python3-psutil \
               python3-ruamel-yaml

# Buildstream plugins base dependencies
dnf install -y git bzr tar lzip patch

# Buildstream utilities
dnf install -y bash-completion

# BuildStream python dependencies
pip3 install blessings Click jinja2 pluginbase ruamel.yaml setuptools

# BuildStream plugin python dependencies
pip3 install arpy

# Buildstream external plugins base dependencies
dnf install -y quilt

# Test suite dependencies
pip3 install apipkg coverage==4.4.0 execnet pep8 pytest pytest-cache pytest-cov \
     pytest-datafiles pytest-env pytest-pep8 pytest-xdist pytest-pylint \
     setuptools_scm

# Needed for CI operations and build docs
dnf install -y make findutils

# Useful for running benchmarks.
dnf install -y time

# Once OSTree is installed, DNF's 'protected package' feature will refuse
# to remove it again due to systemd-udev depending on it, and systemd-udev
# being somehow protected.
#
# The BuildStream test suite expects to be able to `dnf erase ostree` to
# test that the fallback Unix backend doesn't try to use OSTree anywhere
# so we need it to be possible.
#
# This change makes the container vulnerable to running `dnf erase dnf`
# but hey, it's a container, you get to keep the pieces.
echo 'protected_packages=[]' >> /etc/dnf/dnf.conf
