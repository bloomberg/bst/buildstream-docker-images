#!/bin/sh

set -eu

echo "Removing Git"
dnf remove -y git

echo "Removing DNF cache"
dnf clean all

rm -R /root/*
