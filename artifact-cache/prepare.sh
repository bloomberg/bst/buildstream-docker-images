#!/bin/sh

set -eu

dnf install -y nginx openssh-server ostree python3-gobject python3-psutil
dnf install -y python3-ruamel-yaml

# Git is needed so `pip3 install .` can detect the version number of
# the BuildStream package. We remove it in the cleanup.sh script as
# we save 90MB that way.
dnf install -y git

# BuildStream dependencies
pip3 install blessings Click jinja2 MarkupSafe pluginbase ruamel.yaml

useradd artifacts --groups nginx

# Prepare the mount point for the config volume
mkdir /home/artifacts/.config
chown artifacts:artifacts /home/artifacts/.config

# Prepare the mount point for the data volume
mkdir /data
chown artifacts:artifacts /data

# Make it so that we can run nginx as the 'artifacts' user
chmod -R g+rwx /var/lib/nginx
chmod -R g+rwx /var/log/nginx
mkdir /var/run/nginx
chmod g+w /var/run/nginx
chown nginx:nginx /var/run/nginx
